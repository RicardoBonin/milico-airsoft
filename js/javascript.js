/*$(document).ready(function(){
   	$("label").click(function(){
        $("#menu").css("display","none")
    });
	$(window).on('resize', function(){ 
		$("#menu").toggle()
	});
});*/
$(document).ready(function(){
	let toggle = $('#menu').css('margin-top') === '-500px' ? true : false
	$('label').on('click', function() {
 		toggle = !toggle
 		if(toggle) {
  			$('#menu').animate({'margin-top': '-500%'}, 100, "linear")
 		} else {
   			$('#menu').animate({'margin-top': '0px'}, 100, "linear")
 		}
	});
	$(window).on('resize', function(){ 
		var win = $(this); 
		console.log(win.width())
		if (win.width() >= 600) {
			$('#menu').animate({'margin-top': '0px'}, 100, "linear")
		}else{
			$('#menu').animate({'margin-top': '-500%'}, 100, "linear")
		}
	});
});

